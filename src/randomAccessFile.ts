import * as fs from 'fs';
import * as constants from 'constants';
import { Future } from 'funfix';
import { toFuture } from './util';

/**
 * Follow the recommendations from NodeJS file system
 * No accessibility check is performed before reading and opening. Instead, reading and opening are performed directly and errors are handled if the file is not accessible.
 */

/**
 * Reading or writing to a file using random offsets and lengths
 */
export class RandomAccessFile {
  private static close(fd: number): Future<void> {
    return toFuture<void>((cb) => fs.close(fd, (error) => {
      if (error) {
        throw error;
      } else {
        cb((function () { })());
      }
    }))
  }

  constructor(public path: string) { }

  public read(length: number, offset: number = 0): Future<Buffer> {
    return this.open()
      .flatMap((fd) =>
        toFuture<Buffer>((cb) =>
          fs.read(fd, Buffer.allocUnsafe(length), 0, length, offset, (readingError, bytesRead, buffer) => {
            if (readingError) {
              throw readingError;
            } else {
              fs.close(fd, (closingError) => {
                if (closingError) {
                  throw closingError;
                } else {
                  cb(buffer);
                }
              });
            }
          })));
  }

  public write(buffer: Buffer, offset: number = 0, writeToNew: boolean = false): Future<number> {
    return this.open(new OpenModeConfig(writeToNew))
      .flatMap((fd) =>
        toFuture((cb) =>
          fs.write(fd, buffer, 0, buffer.length, offset, (writingError, bytesWritten, buffer) => {
            if (writingError) {
              throw writingError;
            } else {
              fs.close(fd, (closingError) => {
                if (closingError) {
                  throw closingError;
                } else {
                  cb(bytesWritten);
                }
              })
            }
          })));
  }

  private open(mode: OpenModeConfig = new OpenModeConfig()): Future<number> {
    return toFuture<number>((cb) => fs.open(this.path, mode.toFlags(), (error, fd) => {
      if (error) {
        throw error;
      } else {
        cb(fd);
      }
    }));
  }
}

class OpenModeConfig {
  constructor(public create: boolean = false) { }

  public toFlags() {
    return this.create ? constants.O_RDWR | constants.O_CREAT : constants.O_RDWR;
  }
}
