import { Future, Scheduler, Duration, Try, Cancelable, FutureMaker, Success } from "funfix";
import { StringDecoder } from 'string_decoder';

export function toFuture<T>(f: (cb: (_: T) => void) => void, ec: Scheduler = Scheduler.global.get()): Future<T> {
  const m = FutureMaker.empty<T>();
  const task = ec.scheduleOnce(0, () => {
    Try.of(() => f((_) => m.complete(Success(_))))
      .fold((_) => m.failure(_), (_) => { });
  });
  return m.future(task);
}

export function stringDecoder() {
  return new StringDecoder();
}

export function uintToBuffer(value: number, size: number): Buffer {
  const buffer = new Buffer(size);
  buffer.writeUIntBE(value, 0, size);
  return buffer;
}
