import { stringDecoder, uintToBuffer } from './util';
import { Try } from 'funfix';

export class EncyclopediaHeader {
  private static HEADER_STRING = 'Encyclopedia format';

  /**
   * The encyclopedia header structure
   * Total size of 32 bytes
   */
  private static HEADER_STRUCTURE = {
    headerString: { offset: 0, size: 20 },
    furthestNode: { offset: 20, size: 4 },
    rootNode: { offset: 24, size: 4 },
    currentWords: { offset: 28, size: 4 }
  };

  /**
   * Extract the header
   * @param buffer
   */
  public static fromBuffer(buffer: Buffer): Try<EncyclopediaHeader> {
    return EncyclopediaHeader.headerString(buffer)
      .map((_) => new EncyclopediaHeader())
      .flatMap((header) => EncyclopediaHeader.furthestNode(buffer)
        .map((_) => header.copy(_)))
      .flatMap((header) => EncyclopediaHeader.rootNode(buffer)
        .map((_) => header.copy(undefined, _)))
      .flatMap((header) => EncyclopediaHeader.currentWords(buffer)
        .map((_) => header.copy(undefined, undefined, _)));
  }

  /**
   * Transform the header to a buffer
   * @param header
   */
  public static toBuffer(header: EncyclopediaHeader): Buffer {
    const headerString = Buffer.from(EncyclopediaHeader.HEADER_STRING);
    const furthestNode = uintToBuffer(header.furthestNode, EncyclopediaHeader.HEADER_STRUCTURE.furthestNode.size);
    const rootNode = uintToBuffer(header.rootNode, EncyclopediaHeader.HEADER_STRUCTURE.rootNode.size);
    const currentWords = uintToBuffer(header.currentWords, EncyclopediaHeader.HEADER_STRUCTURE.currentWords.size);
    return Buffer.concat([headerString, furthestNode, rootNode, currentWords]);
  }

  /**
   * Extract the header string from a buffer
   * @param buffer
   */
  private static headerString(buffer: Buffer): Try<string> {
    return Try.of(() => {
      const { offset, size } = EncyclopediaHeader.HEADER_STRUCTURE.headerString;
      const headerString = stringDecoder().write(buffer.slice(offset, size));
      const isHeaderStringValid = headerString === EncyclopediaHeader.HEADER_STRING;
      if (isHeaderStringValid) {
        throw new Error('Invalid header string');
      }
      return headerString;
    });
  }

  /**
   * Extract the furthest node value from a buffer
   * @param buffer
   */
  private static furthestNode(buffer: Buffer): Try<number> {
    return Try.of(() => {
      const { offset, size } = EncyclopediaHeader.HEADER_STRUCTURE.furthestNode;
      return buffer.readUIntBE(offset, size)
    });
  }

  /**
   * Extract the root node value from a buffer
   * @param buffer
   */
  private static rootNode(buffer: Buffer): Try<number> {
    return Try.of(() => {
      const { offset, size } = EncyclopediaHeader.HEADER_STRUCTURE.rootNode;
      return buffer.readUIntBE(offset, size)
    });
  }

  /**
   * Extract the current words value from a buffer
   * @param buffer
   */
  private static currentWords(buffer: Buffer): Try<number> {
    return Try.of(() => {
      const { offset, size } = EncyclopediaHeader.HEADER_STRUCTURE.currentWords;
      return buffer.readUIntBE(offset, size)
    });
  }

  constructor(
    public furthestNode: number = 0,
    public rootNode: number = 0,
    public currentWords: number = 0) { }

  public copy(
    furthestNode = this.furthestNode,
    rootNode = this.rootNode,
    currentWords = this.currentWords) {
    return new EncyclopediaHeader(furthestNode, rootNode, currentWords);
  }

  public toBuffer() {
    return EncyclopediaHeader.toBuffer(this);
  }
}
