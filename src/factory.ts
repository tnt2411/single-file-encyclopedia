import { Future } from 'funfix';
import { Encyclopedia } from './encyclopedia';

/**
 * The encyclopedia factory
 */
export class EncyclopediaManager {
  public create(path: string): Future<Encyclopedia> {
    return Encyclopedia.create(path);
  }

  public open(path: string): Future<Encyclopedia> {
    return Encyclopedia.open(path);
  }
}
