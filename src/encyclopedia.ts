import { Future, Try } from 'funfix';
import { RandomAccessFile } from './randomAccessFile';
import { EncyclopediaHeader } from './header';

export class Encyclopedia {
  /**
   * Opening an existing encyclopedia
   * @param path
   */
  public static open(path: string): Future<Encyclopedia> {
    return Encyclopedia.header(path)
      .map((_) => new Encyclopedia(path, _));
  }

  /**
   * Create a new encyclopedia
   * - Write the initial header to file
   * @param path
   */
  public static create(path: string): Future<Encyclopedia> {
    const header = new EncyclopediaHeader();
    const file = new RandomAccessFile(path);
    // FIXME: make sure this does not overwrite existing files
    return file.write(header.toBuffer(), 0, true)
      .map((_) => new Encyclopedia(path, header));
  }

  /**
   * Extract the header from the given file
   * @param path
   */
  private static header(path: string): Future<EncyclopediaHeader> {
    const file = new RandomAccessFile(path);
    return file.read(32)
      .flatMap((_) => Future.fromTry(EncyclopediaHeader.fromBuffer(_)));
  }

  constructor(public path: string, public header: EncyclopediaHeader) { }

  public add(word: string, explanation: string): Future<Encyclopedia> { }

  public delete(word: string): Future<Encyclopedia> { }

  public update(word: string, explanation: string): Future<Encyclopedia> { }

  public search(word: string): Future<string> { }
}
