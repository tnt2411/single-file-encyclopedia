# Assumptions

- Each character is an ASCII UTF-8 character, 1 byte.

# Design

## File structure

- An encyclopedia file consists of:

  - Offset: 0

  - A file header:

|Offset | Size | Description                                                   |
|-------|------|---------------------------------------------------------------|
|0      | 20   | The header string: "Encyclopedia format\000" in UTF-8 encoding|
|20     | 4    | Furthest node number, a big edian unsigned integer            |
|24     | 4    | Root node number, a big edian unsigned integer                |
|28     | 4    | Current number of words, a big edian unsigned integer         |

  - A padding part:

    - Size: 28 bytes

  - A data part:

    - Offset: 60

    - A red-black tree

    - Each node has the size of 1037 + 2^10^10^10 bytes and consists of:

|Offset | Size       | Description                                                               |
|-------|------------|---------------------------------------------------------------------------|
|0      | 1024       | word or phrase                                                            |
|1024   | 1          | the first bit indicates the node color: 0 for black, 1 for red.           |
|       |            | the second bit indicates whether this is a root node: 0 for no, 1 for yes.|
|       |            | the third bit indicates whether this has a left child                     |
|       |            | the forth bit indicates whether this has a right child                    |
|1025   | 4          | parent's page number                                                      |
|1029   | 4          | left child's page number                                                  |
|1033   | 4          | right child's page number                                                 |
|1037   | 2^10^10^10 | explanation                                                               |

## Flows

- Create

  - Initialize with default values and write to disk.

    - When the given path is not accessible, throw an error.

    - When a file already exists under the given path, throw an error.

    - Otherwise, proceed.

  - Return the encyclopedia interface.

- Open

  - Check whether file path exists.

    - When not exist, throw an error.

    - When the given path is not accessible, throw an error.

    - Otherwise, proceed.

  - Read file header into memory and check file structure integrity.

    - When ill-formatted, throw an exception and stop.

    - Otherwise, proceed.

  - Return the encyclopedia interface.

- Search

  - Perform a search on the data part.

    - When a match is found, use the offset to read from the data part and return the explanation.

    - Otherwise, return a special value representing no match.

- Add

  - Create a new node and append it to the data part. Make the color of the node as red.

  - Perform a standard BST insertion.

  - Resolve violations in the tree.

  - Update the file header.

    - The furthest node number

    - The current number of words.

    - The root node.

- Delete

  - Perform a standard BST deletion.

  - Resolve violation.

  - Update the file header

    - The current number of words.

    - The root node.

- Modify

  - Perform a binary search for the given word.

    - When there is a match, throw an exception and stop.

    - Otherwise, proceed.

  - Replace the explanation in the data part.

# Further work

- Finish the implementation of the current design.

- Add automated tests: unit, integration and e2e.

- Employ overflow pages to reduce the size of each nodes.

- Employ a list of free nodes (deleted nodes) to reuse their space.

- Compare B-tree and Red Black tree in more details as B-tree seems to suit this use case better.
